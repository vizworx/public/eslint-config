# Vizworx ESLint Config@vizworx/eslint-config

This monorepo provides common linting configurations used at VizworX for Javascript-based projects. It includes a base package (`@vizworx/eslint-config`) as well as one with additional React-specific rules (`@vizworx/eslint-config-react`).

Both are based on [eslint-config-airbnb](https://npmjs.com/eslint-config-airbnb).
