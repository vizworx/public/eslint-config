module.exports = {
  extends: [
    'eslint-config-airbnb',
    'eslint-config-airbnb/hooks',
    '@vizworx/eslint-config/vizworx',
    './vizworx',
  ].map(require.resolve),
};
