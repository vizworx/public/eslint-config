# @vizworx/eslint-config-react

This package provides a config with React-specific linting rules used at VizworX, that is based upon [eslint-config-airbnb](https://npmjs.com/eslint-config-airbnb) and [@vizworx/eslint-config](https://npmjs.com/@vizworx/eslint-config).

## Install

### npm

```sh
npx install-peerdeps --dev @vizworx/eslint-config-react
```

### Yarn 2:

```sh
$(yarn dlx install-peerdeps -Y --dev @vizworx/eslint-config-react --dry-run | grep 'yarn add' | sed 's/--registry .*//')
```

### Yarn 1:

```sh
npx install-peerdeps -Y --dev @vizworx/eslint-config-react
```

## Usage

### .eslintrc

```json
{
  "extends": ["@vizworx/eslint-config-react"]
}
```
