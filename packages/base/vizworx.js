module.exports = {
  parser: require.resolve('@babel/eslint-parser'),
  env: { browser: true },
  rules: {
    'no-console': ['error', { allow: ['error'] }],
    'no-multiple-empty-lines': ['error', { max: 1, maxBOF: 0 }],
    'object-curly-newline': ['error', {
      minProperties: 8,
      multiline: true,
      consistent: true,
    }],
  },
  overrides: [
    {
      files: [
        '**/spec.js',
        '**/*.spec.js',
      ],
      env: { jest: true },
    },
  ],
};
