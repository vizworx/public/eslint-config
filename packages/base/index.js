module.exports = {
  extends: [
    'eslint-config-airbnb-base',
    './vizworx',
  ].map(require.resolve),
};
