const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const readFile = util.promisify(require('fs').readFile);

// The order of peers matters in case of conflicts. @babel/eslint-parser has a
// more restrictive peer dependency than eslint-config-airbnb
const packages = {
  base: {
    deps: ['eslint-config-airbnb-base'],
    peers: ['eslint-config-airbnb-base', '@babel/eslint-parser'],
  },
  react: {
    deps: ['eslint-config-airbnb'],
    peers: ['eslint-config-airbnb', '@babel/eslint-parser'],
  },
};

const sequentialPromise = (callbacks) => callbacks.reduce(
  (acc, next) => acc.then(next),
  Promise.resolve(),
);

const updatePeers = async (package, opts) => {
  // Use npm to get the peer dependencies
  const raw = await exec(`npm view ${package} peerDependencies --json`)
  // Turn the peer dependencies into something we can use with Yarn
  const formatted = Object.entries(JSON.parse(raw.stdout))
    .map(([package, range]) => `"${package}@${range}"`)
    .join(' ');
  // Update the peer dependencies
  await exec(`yarn add --peer ${formatted}`, opts);
};

const verifyOutputPackage = async (ours) => {
  const opts = { cwd: path.resolve('outputs', ours) };

  const { peerDependencies } = JSON.parse(
    await readFile(path.resolve('packages', ours, 'package.json'), 'utf-8')
  );
  const formatted = Object.entries(peerDependencies)
    .map(([package, range]) => `"${package}@${range}"`)
    .join(' ');

  console.log(`[${ours}] Installing dependencies for outputs package`);
  await exec(`yarn add --dev ${formatted}`, opts);

  // Test that the package runs properly
  console.log(`[${ours}] Verifying that outputs package runs`);
  return exec('yarn start', opts);
};

// Loop over these packages and update them one by one
sequentialPromise(Object.entries(packages).map(([ours, update]) => async () => {
  const { deps = [], peers = [] } = update;
  const opts = { cwd: path.resolve('packages', ours) };

  // Update the dependencies
  await sequentialPromise(deps.map((package) => () => {
    console.log(`[${ours}] Updating ${package}`);
    return exec(`yarn up ${package}`, opts);
  }));

  // Update the peer dependencies
  await sequentialPromise(peers.map((package) => () => {
    console.log(`[${ours}] Installing peers for ${package}`);
    return updatePeers(package, opts);
  }));

  await verifyOutputPackage(ours)
    // We rethrow the error to drop any of the `exec` props
    .catch((e) => { throw new Error(e.message); });
})).catch((err) => {
  console.error('Error:', err);
});
